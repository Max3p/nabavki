﻿using Nabavki.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nabavki.ViewModel
{
    public class PartnerViewModel
    {

        public IEnumerable<Firma> FirmaList { get; set; }
        public Partner Partner { get; set; }
    }
}