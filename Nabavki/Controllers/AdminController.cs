﻿using Nabavki.Models;
using Nabavki.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Nabavki.Controllers
{
    public class AdminController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

#region Firma
        public readonly NabavkiDBEntities _dbContext;
        private object stateId;

        public AdminController()
        {
            _dbContext = new NabavkiDBEntities();
        }

        // GET: Admin
        public ActionResult FirmaIndex()
        {
            var firmaList = _dbContext.Firmas.ToList();
            return View(firmaList);
        }
        // GET: Admin
        public ActionResult AddFirma()
        {
            return View();
        }
 
        [HttpPost]
        public ActionResult SaveFirma(Firma firma)
        {
            _dbContext.Firmas.Add(firma);
           
            _dbContext.SaveChanges();

            return RedirectToAction("FirmaIndex", "Admin");
        }
    
        public ActionResult DeleteFirma(int id)
        {
            Firma firma = _dbContext.Firmas.Find(id);
            _dbContext.Firmas.Remove(firma);
            _dbContext.SaveChanges();
            return RedirectToAction("FirmaIndex");
        }
       
        public ActionResult EditFirma(int? id)
        {
            if(id == null)
            {
                return HttpNotFound();
            }
            Firma firma = _dbContext.Firmas.Find(id);
            if(firma == null)
            {
                return HttpNotFound();
            }
            return View(firma);
        }
        [HttpPost]
        public ActionResult EditFirma([Bind(Include = "IdFirma, Naziv")] Firma firma)
        {
            if (ModelState.IsValid)
            {
                _dbContext.Entry(firma).State = EntityState.Modified;
                _dbContext.SaveChanges();
                return RedirectToAction("FirmaIndex");
            }
            return View(firma);
        }
#endregion  End Of Firma


        #region Partner

        public ActionResult PartnerIndex()
        {
            var partnerList = _dbContext.Partners.ToList();
            return View(partnerList);
        }

        public ActionResult AddPartner()
        {
            var firme = _dbContext.Firmas;

            var viewModel = new PartnerViewModel()
            {
                Partner = new Partner(),
                FirmaList = firme
            };

            return View("AddPartner", viewModel);
        }

        [HttpPost]
        public ActionResult SavePartner(Partner partner)
        {
            if (!ModelState.IsValid)
            {
                var viewModel = new PartnerViewModel
                {
                    Partner = partner,
                    FirmaList = _dbContext.Firmas.ToList()
                };
                return View("AddPartner", viewModel);
            }
            if(partner.IdPartner == 0)
            {
                _dbContext.Partners.Add(partner);
            }
            else
            {
                var partnerDb = _dbContext.Partners.SingleOrDefault(x => x.IdPartner == partner.IdPartner);

                partnerDb.Naziv = partner.Naziv;
                partnerDb.IdFirma = partner.IdFirma;
            }
         
            
            _dbContext.SaveChanges();

            return RedirectToAction("PartnerIndex", "Admin");
        }

        public ActionResult EditPartner(int id)
        {
            var partner = _dbContext.Partners.SingleOrDefault(i => i.IdPartner == id);
            if(partner == null)
            {
                return HttpNotFound();
            }
            return View("EditPartner", new PartnerViewModel
            {
                Partner = partner,
                FirmaList = _dbContext.Firmas.ToList()
            });
          
           
        }

        public ActionResult DeletePartner(int id)
        {
            Partner partner = _dbContext.Partners.Find(id);
            _dbContext.Partners.Remove(partner);
            _dbContext.SaveChanges();
            return RedirectToAction("PartnerIndex");
        }
        #endregion End Of Partner




        #region Artical
        public ActionResult ArticalIndex(string searchString)
        {
            var artical = from a in _dbContext.Artiklis
                          select a;
            if (!String.IsNullOrEmpty(searchString))
            {
                artical = artical.Where(s => s.Naziv.Contains(searchString));
            }
            
            return View(artical);
        }
      
        public ActionResult AddArtical()
        {
            var partneri = _dbContext.Partners.ToList();
            var firme = _dbContext.Firmas.ToList();
            
            var viewModel = new ArticalViewModel()
            {
                Artikli = new Artikli(),

                Firma = firme,
                Partner = partneri,
                
                
            };


            return View("AddArtical", viewModel);
        }
        [HttpPost]
        public ActionResult SaveArtical(Artikli artikli)
        {
            if (!ModelState.IsValid)
            {
                var viewModel = new ArticalViewModel
                {
                    Partner = _dbContext.Partners.ToList(),
                    Artikli = artikli
                    
                };

                return View("AddArtical", viewModel);
            }
            if (artikli.IdArtikli == 0)
            {
                var artiklis = new Artikli();
                    
                _dbContext.Artiklis.Add(artikli);
            }
            else
            {
                var artDb = _dbContext.Artiklis.SingleOrDefault(x => x.IdArtikli == artikli.IdArtikli);
                artDb.Naziv = artikli.Naziv;
                artDb.Sifra = artikli.Sifra;
                artDb.IdPartner = artikli.IdPartner;
            }
            _dbContext.SaveChanges();
            return RedirectToAction("ArticalIndex", "Admin");
        }


        public ActionResult EditArtical(int id)
        {
            var artical = _dbContext.Artiklis.SingleOrDefault(i => i.IdArtikli == id);
                if(artical == null)
                   {
                     return HttpNotFound();
                    }
            return View("EditArtical", new ArticalViewModel
            {
                Artikli = artical,
                Partner = _dbContext.Partners.ToList()
            });
        }



        public ActionResult DeleteArtical(int id)
        {
            Artikli art = _dbContext.Artiklis.Find(id);
            _dbContext.Artiklis.Remove(art);
            _dbContext.SaveChanges();
            return RedirectToAction("ArticalIndex");
        }

  
        public ActionResult GetPartnerList(int firma)
        {
            var partners = _dbContext.Partners
                                        .Where(c => c.IdFirma == firma).ToList()
                                        .Select(x => new PartnersViewModel
                                        {
                                            IdPartner = x.IdPartner,
                                            Naziv = x.Naziv

                                        });


            return Json(partners, JsonRequestBehavior.AllowGet);
        }
        public class PartnersViewModel
        {
            public int IdPartner { get; set; }
            public string Naziv { get; set; }
        }
        public ActionResult GetFirmas(int firma)
        {
            var partners = _dbContext.Firmas
                                        .Where(c => c.IdFirma == firma).ToList();
                                        


            return Json(partners, JsonRequestBehavior.AllowGet);
        }


        #endregion
    }
}